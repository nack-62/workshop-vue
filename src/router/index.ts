import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import AddBook from '@/views/AddBook.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    sensitive: true,
    component: HomeView
  },
  {
    path: '/add-book',
    name: 'add-book',
    component: AddBook
  },
  {
    path: '/edit-book/:id',
    name: 'edit-book',
    component: AddBook
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
