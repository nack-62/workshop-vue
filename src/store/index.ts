import { createStore } from 'vuex'


export default createStore( {
  state: {
    books: []
  },
  mutations: {
    addBook ( state, payload ) {
      state.books.push( payload );
      
    },
    editBook ( state, { data, index } ) {
      state.books[ index ] = data;
      
    },
    deleteBook( state, index ) {
      state.books.splice(index,1)
    }
  },
  actions: {
    addBook ( { commit }, payload ) {
      commit( 'addBook', payload )
    },
    editBook ( { commit }, payload ) {
      commit( 'editBook', payload )
    },
    deleteBook( { commit }, payload ) {
      commit( 'deleteBook', payload )
    }
  }
} )
